// # spell-checker:ignore APPNAME
/* eslint-env es6, node */

import path from 'path';
import { inspect } from 'util';

import _ from 'lodash';

import appPaths from '..';

// Extend appPaths with a "log" location
// eslint-disable-next-line functional/immutable-data , @typescript-eslint/no-explicit-any
(appPaths as any).log = function ({ isolated = appPaths.$isolated() } = {}) {
  const options = { isolated };
  const useIsolated =
    options.isolated === undefined || options.isolated === null
      ? // eslint-disable-next-line functional/no-this-expression
        this.$isolated()
      : options.isolated;
  return path.join(
    // eslint-disable-next-line functional/no-this-expression
    this.state(options),
    // eslint-disable-next-line functional/no-this-expression
    (useIsolated ? '' : this.$name() + '-') + 'log'
  );
};

console.log('appPaths:', inspect(appPaths));
if (_) {
  _.each(appPaths, (_value, key) => {
    console.log(key, '=', appPaths[key]());
  });
}
// # console.log('appPaths.log(false):', appPaths.log(false));
// # console.log('appPaths.log(true):', appPaths.log(true));

// eslint-disable-next-line functional/immutable-data
delete process.env.XDG_CONFIG_HOME;
// eslint-disable-next-line functional/no-let
let p = appPaths('dross');

console.log('p:', inspect(p));
if (_) {
  _.each(p, (_value, key) => {
    console.log(key, '=', p[key]());
  });
}

p = appPaths({ suffix: '-nodejs' });

console.log('p:', inspect(p));
if (_) {
  _.each(p, (_value, key) => {
    console.log(key, '=', p[key]());
  });
}

p = appPaths({ name: 'extraordinaire', suffix: '-nodejs' });

console.log('p:', inspect(p));
if (_) {
  _.each(p, (_value, key) => {
    console.log(key, '=', p[key]());
  });
}

p = appPaths({ name: 'fluffy', isolated: false });

console.log('p:', inspect(p));
if (_) {
  _.each(p, (_value, key) => {
    console.log(key, '=', p[key]());
  });
}
