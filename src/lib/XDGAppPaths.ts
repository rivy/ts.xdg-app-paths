/**
 * Multiplies a value by 2. (Also a full example of TypeDoc's functionality.)
 *
 * ### Example (es module)
 * ```js
 * import { double } from 'typescript-starter'
 * console.log(double(4))
 * // => 8
 * ```
 *
 * ### Example (commonjs)
 * ```js
 * var double = require('typescript-starter').double;
 * console.log(double(4))
 * // => 8
 * ```
 *
 * @param value - Comment describing the `value` parameter.
 * @returns Comment describing the return type.
 * @anotherNote Some other value.
 */
export const double = (value: number) => {
  return value * 2;
};

/**
 * Raise the value of the first parameter to the power of the second using the
 * es7 exponentiation operator (`**`).
 *
 * ### Example (es module)
 * ```js
 * import { power } from 'typescript-starter'
 * console.log(power(2,3))
 * // => 8
 * ```
 *
 * ### Example (commonjs)
 * ```js
 * var power = require('typescript-starter').power;
 * console.log(power(2,3))
 * // => 8
 * ```
 * @param base - the base to exponentiate
 * @param exponent - the power to which to raise the base
 */
export const power = (base: number, exponent: number) => {
  /**
   * This es7 exponentiation operator is transpiled by TypeScript
   */
  return base ** exponent;
};

// ====

// spell-checker: ignore () LOCALAPPDATA APPDATA tempdir

import path from 'path';

import osPaths from 'os-paths';
import xdg from 'xdg-portable';

const isWinOS = /^win/i.test(process.platform);

type Options = {
  readonly name?: string | null;
  readonly suffix?: string | null;
  readonly isolated?: boolean | null;
};

type methodOptions = {
  readonly isolated?: boolean | null;
};

type basicXDGAppPaths = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  readonly [index: string]: any;
  readonly cache: () => string;
  readonly config: () => string;
  readonly data: () => string;
  readonly runtime: () => string | undefined;
  readonly state: () => string;
  readonly configDirs: () => readonly string[];
  readonly dataDirs: () => readonly string[];
};

type XDGAppPaths = basicXDGAppPaths & {
  new (options?: Options | string): XDGAppPaths;
  (options?: Options | string): XDGAppPaths;
  readonly $name: () => string;
  readonly $isolated: () => boolean;
};

const base = (name: string, _isolated?: boolean | null) => {
  const cache = ({ isolated = _isolated }: methodOptions = {}) => {
    const options = { isolated };
    return path.join(xdg.cache(), options.isolated ? name : '');
  };

  const config = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    return path.join(xdg.config(), options.isolated ? name : '');
  };

  const data = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    return path.join(xdg.data(), options.isolated ? name : '');
  };

  const runtime = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    return xdg.runtime()
      ? path.join(xdg.runtime() as string, options.isolated ? name : '')
      : undefined;
  };

  const state = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    return path.join(xdg.state(), options.isolated ? name : '');
  };

  const configDirs = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    return xdg
      .configDirs()
      .map((s) => path.join(s, options.isolated ? name : ''));
  };

  const dataDirs = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    return xdg
      .dataDirs()
      .map((s) => path.join(s, options.isolated ? name : ''));
  };

  return {
    cache,
    config,
    data,
    runtime,
    state,
    configDirs,
    dataDirs,
  } as basicXDGAppPaths;
};

const windows = (name: string, _isolated?: boolean | null) => {
  const { env } = process;
  const homedir = osPaths.home();
  const tempdir = osPaths.temp();

  // # ref: <https://www.thewindowsclub.com/local-localnow-roaming-folders-windows-10> @@ <http://archive.is/tDEPl>
  const appData =
    env.APPDATA ?? path.join(homedir ?? tempdir, 'AppData', 'Roaming'); // APPDATA == "AppData/Roaming" contains data which may follow user between machines
  const localAppData =
    env.LOCALAPPDATA ?? path.join(homedir ?? tempdir, 'AppData', 'Local'); // LOCALAPPDATA == "AppData/Local" contains local-machine-only user data

  // Locations for data/config/cache/state are invented (Windows doesn't have a popular convention)

  const cache = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    return !options.isolated || env.XDG_CACHE_HOME
      ? path.join(xdg.cache(), options.isolated ? name : '')
      : path.join(localAppData, options.isolated ? name : '', 'Cache');
  };

  const config = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    const config =
      !options.isolated || env.XDG_CONFIG_HOME
        ? path.join(xdg.config(), options.isolated ? name : '')
        : path.join(appData, options.isolated ? name : '', 'Config');
    return config;
  };

  const data = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    const data =
      !options.isolated || env.XDG_DATA_HOME
        ? path.join(xdg.data(), options.isolated ? name : '')
        : path.join(appData, options.isolated ? name : '', 'Data');
    return data;
  };

  const runtime = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    return xdg.runtime()
      ? path.join(xdg.runtime() as string, options.isolated ? name : '')
      : undefined;
  };

  const state = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    return !options.isolated || env.XDG_STATE_HOME
      ? path.join(xdg.state(), options.isolated ? name : '')
      : path.join(localAppData, options.isolated ? name : '', 'State');
  };

  const configDirs = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    const dirs = [config(options)];
    if (env.XDG_CONFIG_DIRS) {
      // eslint-disable-next-line functional/immutable-data
      dirs.push(
        ...env.XDG_CONFIG_DIRS.split(path.delimiter).map((s) =>
          path.join(s, options.isolated ? name : '')
        )
      );
    }

    return dirs;
  };

  const dataDirs = function ({ isolated = _isolated }: methodOptions = {}) {
    const options = { isolated };
    const dirs = [data(options)];
    if (env.XDG_DATA_DIRS) {
      // eslint-disable-next-line functional/immutable-data
      dirs.push(
        ...env.XDG_DATA_DIRS.split(path.delimiter).map((s) =>
          path.join(s, options.isolated ? name : '')
        )
      );
    }

    return dirs;
  };

  return {
    cache,
    config,
    data,
    runtime,
    state,
    configDirs,
    dataDirs,
  } as basicXDGAppPaths;
};

// eslint-disable-next-line functional/no-class
class _XDGAppPaths {
  readonly fn: XDGAppPaths;
  constructor(options: Options = {}) {
    // ? const options = {name, suffix, isolated};
    const XDGAppPaths = function (options: Options = {}) {
      return new _XDGAppPaths(options).fn;
    };

    // eslint-disable-next-line functional/no-this-expression
    this.fn = XDGAppPaths as XDGAppPaths;

    options = options ?? {};
    if (typeof options !== 'object') {
      options = { name: options };
    }

    // eslint-disable-next-line functional/no-let
    let name = options.name ?? '';

    const suffix = options.suffix ?? '';

    const isolated =
      options.isolated === undefined || options.isolated === null
        ? true
        : options.isolated;

    if (!name) {
      // Find a suitable application name (ref: <https://stackoverflow.com/a/46110961/43774>)
      name = path.parse(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (process as any).pkg
          ? process.execPath
          : require.main
          ? require.main.filename
          : process.argv[0]
      ).name;
    }

    if (suffix) {
      name += suffix;
    }

    // Connect to platform-specific API functions by extension
    const extension = isWinOS ? windows(name, isolated) : base(name, isolated);
    Object.keys(extension).forEach((key) => {
      // eslint-disable-next-line functional/no-this-expression , @typescript-eslint/no-explicit-any
      (this.fn as any)[key] = extension[key];
    });

    // eslint-disable-next-line functional/no-this-expression , @typescript-eslint/no-explicit-any
    (this as any).fn.$name = () => name;
    // eslint-disable-next-line functional/no-this-expression , @typescript-eslint/no-explicit-any
    (this as any).fn.$isolated = () => isolated;
  }
}

export default new _XDGAppPaths().fn;
